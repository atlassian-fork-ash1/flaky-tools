package com.atlassian.test.rules;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Flaky {
    String jiraIssue();

    /**
     * Must conform to the format:
     *   YYYY-MM-DD
     */
    String expiryDate();
}
