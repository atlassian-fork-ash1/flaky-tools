package com.atlassian.test.rules;

import org.junit.Assume;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Optional;

import static org.junit.Assert.fail;

public class IgnoreFlakes implements TestRule {

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                Optional<Flaky> annotation = getAnnotation(description);

                if (shouldRunTest(annotation)) {
                    base.evaluate();
                } else if (testIsExpired(annotation)) {
                    fail("Flaky test has expired. Delete or fix.");
                }

                Assume.assumeTrue("Ignoring Flaky test.", false);
            }
        };
    }

    private boolean shouldRunTest(Optional<Flaky> optionalAnnotation) {
        boolean flakey_test = optionalAnnotation.isPresent();
        boolean flakey_run = System.getProperty("run.flaky.on") != null;
        return (flakey_test == flakey_run);
    }

    private Optional<Flaky> getAnnotation(Description description) {
        try {
            Optional<Flaky> annotation = Optional.ofNullable(description.getAnnotation(Flaky.class));

            if (!annotation.isPresent()) {
                annotation = Optional.ofNullable(description.getTestClass().getAnnotation(Flaky.class));
            }

            return annotation;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static final SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");

    private boolean testIsExpired(Optional<Flaky> optionalAnnotation) {
        Calendar cal = Calendar.getInstance();

        return optionalAnnotation.
                map(a -> {
                    try {
                        return cal.getTime().after(sdf.parse(a.expiryDate()));
                    } catch (Exception e) {
                        return false;
                    }
                }).
                orElse(false);
    }
}