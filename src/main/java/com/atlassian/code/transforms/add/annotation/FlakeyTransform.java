package com.atlassian.code.transforms.add.annotation;

import spoon.Launcher;
import spoon.processing.Processor;

import java.io.File;
import java.util.function.Consumer;

public class FlakeyTransform implements Consumer<FlakeyTransform.Params> {

    // think struct
    public static class Params {
        public String srcRoot;
        public String className;
        public String methodName;
        public String jiraIssueKey;
        public String timeStamp;
    }

    @Override
    public void accept(Params params) {
        Launcher launcher = new Launcher();
        File file = new File(params.srcRoot + "/" + params.className.replace('.', '/') + ".java").getAbsoluteFile();
        launcher.addInputResource(file.getPath());
        launcher.getEnvironment().setNoClasspath(true);
        launcher.getEnvironment().setAutoImports(true);
        launcher.setSourceOutputDirectory(params.srcRoot);
        Processor<?> processor = new FlakyAnnotationAdder("com.atlassian.Sample", "test");
        launcher.addProcessor(processor);
        launcher.run();
    }
}
