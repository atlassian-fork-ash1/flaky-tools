package com.atlassian.code.transforms.add.annotation;

import com.atlassian.test.rules.Flaky;
import com.atlassian.test.rules.IgnoreFlakes;
import com.atlassian.test.rules.InvertResult;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;

public class TestExpiredFlakes {
    @Rule
    public RuleChain chain = RuleChain.emptyRuleChain()
            .around(new InvertResult())
            .around(new IgnoreFlakes());

    @Test
    @Flaky(jiraIssue = "FAKE-1234", expiryDate = "2000-01-01")
    public void FailsExpiredTests() {
        // Firmly in the past, should be failed by IgnoreFlakes
    }
}
