package com.atlassian.code.transforms.add.annotation;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class TestFlakey {
    private final String expected = "" +
            "package com.atlassian;\n" +
            "\n" +
            "\n" +
            "import com.atlassian.test.rules.Flaky;\n" +
            "import java.util.Random;\n" +
            "import org.junit.Assert;\n" +
            "import org.junit.Test;\n" +
            "\n" +
            "\n" +
            "public class Sample {\n" +
            "    @Test\n" +
            "    @Flaky\n" +
            "    public void test() throws Exception {\n" +
            "        Assert.assertTrue(new Random().nextBoolean());\n" +
            "    }\n" +
            "}\n" +
            "\n" +
            "";

    @Test
    public void AnnotateSampleTest() throws Exception {
        FileUtils.copyDirectory(new File("src/test/fixtures"), new File("tmp/src/test/java"));
        File sample = new File("tmp/src/test/java/com/atlassian/Sample.java");

        String before = FileUtils.readFileToString(sample);

        FlakeyTransform flakey = new FlakeyTransform();
        FlakeyTransform.Params params = new FlakeyTransform.Params();
        params.srcRoot = "tmp/src/test/java";
        params.className = "com.atlassian.Sample";
        params.methodName = "test";
        params.jiraIssueKey = "";
        params.timeStamp = "2025-01-01";
        flakey.accept(params);
        String after = FileUtils.readFileToString(sample);

        assertThat(after, equalTo(expected));
        FileUtils.forceDeleteOnExit(new File("tmp"));
    }
}
